<?php
header('Content-Type: text/html; charset=UTF-8');
session_start();
if (!empty($_SESSION['login'])) {
    header('Location: cookies.php');
}
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
	
    if (!empty($_COOKIE['open_error'])) {
        echo '<div class = "error"> Непревильный логин или пароль!</div>';
        setcookie('login_error', '', 1000);
    }
?>

<!DOCTYPE html>
<html lang="ru">
   <head>
      <style>
        .error{
			border: 2px solid red;
			background-color: red;
		}
      </style>
      <meta charset="utf-8">
      <title>Логин</title>
   </head>
   <body>
      <div>
         <h2>Логин</h2>
         <form action="" method="POST">
            <label> Логин: <br>
            <input name="login" />
            </label>
            <br>
			
            <label> Пароль:  <br>
            <input name="hashpass" />
            </label>
			
            <br>
            <br>
            <input type="submit"value="enter" />
            <br>
         </form>
      </div>
   </body>
</html>

<?php
}
else {
    
    $user = 'u20388';
		$pass = '4543464';
		$db = new PDO('mysql:host=localhost;dbname=u20388', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    try {
        $stmt = $db->prepare("SELECT * FROM Data WHERE login = ? AND hashpass = ?;");
        $stmt->bindValue(1, $_POST['login'], PDO::PARAM_STR);
        $stmt->bindValue(2, md5($_POST['hashpass']), PDO::PARAM_STR);
        $stmt->execute();
    }
    catch(PDOException $e) {
        print ('Ошибка : ' . $e->getMessage());
        exit();
    }
    
    $user_id = $stmt->fetchAll(PDO::FETCH_ASSOC);
    
    if (!empty($user_id)) {
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['hashpass'] = $_POST['hashpass'];
        $_SESSION['id'] = $user_id[0]['id'];
        header('Location: cookies.php');
    } else {
        setcookie('login_error', 1, 0);
        header('Location: login.php');
    }
}
